/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.service;

import net.coalevo.bank.model.Account;
import net.coalevo.bank.model.AccountStats;
import net.coalevo.bank.model.BankServiceException;
import net.coalevo.foundation.model.Agent;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.Maintainable;
import net.coalevo.foundation.model.Service;

/**
 * Defines the interface of the bank service.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface BankService
    extends Service, Maintainable {

  /**
   * Tests if there is an {@link Account} bound to a specified agent.
   *
   * @param caller the requesting {@link Agent}.
   * @param aid    the identifier of the agent the new account should be bound to.
   * @return true if account exists, false otherwise.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   */
  public boolean existsAccount(Agent caller, AgentIdentifier aid)
      throws SecurityException, BankServiceException;

  /**
   * Opens a new {@link Account} bound to a specified agent.
   *
   * @param caller the requesting {@link Agent}.
   * @param aid    the identifier of the agent the new account should be bound to.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   */
  public void openAccount(Agent caller, AgentIdentifier aid)
      throws SecurityException, BankServiceException;

  /**
   * Closes an existing {@link Account}.
   *
   * @param caller the requesting {@link Agent}.
   * @param aid    the identifier of the agent the new account is bound to.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   */
  public void closeAccount(Agent caller, AgentIdentifier aid)
      throws SecurityException, BankServiceException;

  /**
   * Returns the account balance for an existing {@link Account}.
   *
   * @param caller the requesting {@link Agent}.
   * @param aid    the identifier of the agent the new account is bound to.
   * @return the balance of the specified account.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   */
  public long getAccountBalance(Agent caller, AgentIdentifier aid)
      throws SecurityException, BankServiceException;

  /**
   * Transfer between two accounts.
   *
   * @param caller the requesting {@link Agent}.
   * @param from   the identifier of the agent the paying account is bound to.
   * @param to     the identifier of the agent the receiving account is bound to.
   * @param sum    the sum to be transferred.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   * @return the amount of money transferred.
   */
  public long transfer(Agent caller, AgentIdentifier from, AgentIdentifier to, long sum)
      throws SecurityException, BankServiceException;

  /**
   * Deposits a specific sum to a specified account.
   * <p/>
   * Note: At the moment this are means to generate "money". There is no real
   * economic model behind this, given that there is no account where the money
   * is coming from.
   * The method should be guarded by a security policy.
   *
   * @param caller the requesting {@link Agent}.
   * @param to     the identifier of the agent the receiving account is bound to.
   * @param sum    the sum to be awarded.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   */
  public void deposit(Agent caller, AgentIdentifier to, long sum)
      throws SecurityException, BankServiceException;

  /**
   * Retrieves the specified sum from the given account.
   * <p/>
   * This method should be guarded by a security policy.
   *
   * @param caller the requesting {@link Agent}.
   * @param to     the identifier of the agent the receiving account is bound to.
   * @param sum    the sum to be awarded.
   * @throws SecurityException    if the calling {@link Agent} is not authentic or
   *                              not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   * @return the amount retrieved.
   */
  public long retrieve(Agent caller, AgentIdentifier to, long sum)
      throws SecurityException, BankServiceException;

  /**
   * Returns summary {@link AccountStats} for all {@link Account}
   * instances.
   *
   * @param caller     the requesting {@link Agent}.
   * @return a <tt>List</tt> of identifier strings.
   * @throws SecurityException          if the calling {@link Agent} is not authentic or
   *                                    not authorized.
   * @throws BankServiceException if an error occurs while executing this action.
   */
  public AccountStats getAccountStats(Agent caller)
    throws SecurityException, BankServiceException;

}//interface BankService
