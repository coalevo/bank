/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.impl;

import net.coalevo.bank.model.AccountStats;

/**
 * Provides a simple implementation of {@link AccountStats}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AccountStatsImpl
    implements AccountStats {

  private double m_Average;
  private long m_Sum;
  private long m_Min;
  private long m_Max;

  public AccountStatsImpl(long min, long max, long sum, double average) {
    m_Average = average;
    m_Max = max;
    m_Sum = sum;
    m_Min = min;
  }//constructor

  public long getMax() {
    return m_Max;
  }//getMax

  public double getAverage() {
    return m_Average;
  }//getAverage

  public long getSum() {
    return m_Sum;
  }//getSum

  public long getMin() {
    return m_Min;
  }//getMin

}//class AgentBoundCounterStatsImpl
