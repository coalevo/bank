/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.impl;

import net.coalevo.bank.model.AccountStats;
import net.coalevo.bank.model.BankServiceException;
import net.coalevo.bank.model.NoSuchAccountException;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.util.CacheMapExpelHandler;
import net.coalevo.foundation.util.LRUCacheMap;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.sql.ResultSet;
import java.util.*;

/**
 * Provides a manager for accounts, handling instance,
 * creation, retrieval and update with a transparent LRU
 * caching mechanism.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AccountManager
    extends LRUCacheMap<String, AccountImpl> {

  private Marker m_LogMarker = MarkerFactory.getMarker(AccountManager.class.getName());
  private BankStore m_Store;

  public AccountManager(BankStore s, int cachesize) {
    super(cachesize);
    m_Store = s;
    m_ExpelHandler = new ExpelHandler();
  }//AccountManager

  public void createAccount(AgentIdentifier aid)
      throws BankServiceException {

    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());
    params.put("balance", Long.toString(0));
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("createAccount", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createAccount()", ex);
      throw new BankServiceException(ex);
    } finally {
      m_Store.releaseConnection(lc);
    }
  }//createAccount

  public void deleteAccount(AgentIdentifier aid)
      throws BankServiceException {

    Map<String, String> params = new HashMap<String, String>();
    params.put("agent_id", aid.getIdentifier());

    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      lc.executeUpdate("deleteAccount", params);
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"deleteAccount()", ex);
      throw new BankServiceException();
    } finally {
      m_Store.releaseConnection(lc);
    }
    //remove from cache
    remove(aid.getIdentifier());
  }//deleteAccount

  public AccountImpl getAccount(AgentIdentifier aid)
      throws NoSuchAccountException {
    final String cacheid = aid.getIdentifier();
    AccountImpl c = get(cacheid);
    if (c == null) {
      ResultSet rs = null;
      Map<String, String> params = new HashMap<String, String>();
      params.put("agent_id", aid.getIdentifier());

      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        rs = lc.executeQuery("getAccount", params);
        if (rs.next()) {
          c = new AccountImpl(aid, rs.getLong(1));
        }
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"getAccount()", ex);
      } finally {
        SqlUtils.close(rs);
        m_Store.releaseConnection(lc);
      }
      if (c == null) {
        throw new NoSuchAccountException(cacheid);
      }
      put(cacheid, c);
    }
    return c;
  }//getAccount

  public List<String> listAccounts()
      throws BankServiceException {
    //List
    List<String> ids = new ArrayList<String>();
    //Build list
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("listAccounts", null);
      while (rs.next()) {
        ids.add(rs.getString(1));
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"listAccounts()", ex);
      throw new BankServiceException("listAccounts()", ex);
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }
    return ids;
  }//listAccounts

  public AccountStats getAccountStats()
      throws BankServiceException {

    //run a sync to ensure all accounts are updated in the store.
    sync();

    AccountStats abcs = null;
    ResultSet rs = null;
    LibraryConnection lc = null;
    try {
      lc = m_Store.leaseConnection();
      rs = lc.executeQuery("getAccountStats", null);
      if (rs.next()) {
        abcs = new AccountStatsImpl(
            rs.getLong(1), //min
            rs.getLong(2), //max
            rs.getLong(3), //sum
            rs.getDouble(4) //average
        );
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"getAccountStats(String)", ex);
      //TODO: Add message
      throw new BankServiceException();
    } finally {
      SqlUtils.close(rs);
      m_Store.releaseConnection(lc);
    }

    return abcs;
  }//getAccountStats(String)

  public void sync() {
    synchronized (this) {
      for (Iterator iterator = entrySet().iterator(); iterator.hasNext();) {
        Map.Entry entry = (Map.Entry) iterator.next();
        AccountImpl ci = (AccountImpl) entry.getValue();
        updateAccount(ci);
      }
    }
  }//sync

  private void updateAccount(AccountImpl c) {
    if (c.isDirty()) {
      Map<String, String> params = new HashMap<String, String>();
      params.put("agent_id", c.getAgentIdentifier().getIdentifier());
      params.put("balance", Long.toString(c.getBalance()));
      LibraryConnection lc = null;
      try {
        lc = m_Store.leaseConnection();
        lc.executeUpdate("updateAccount", params);
      } catch (Exception ex) {
        Activator.log().error(m_LogMarker,"updateAccount()", ex);
      } finally {
        m_Store.releaseConnection(lc);
      }
      c.setDirty(false);
    }
  }//updateAccount

  class ExpelHandler implements CacheMapExpelHandler<String, AccountImpl> {

    public void expelled(Map.Entry entry) {
      AccountImpl ci = (AccountImpl) entry.getValue();
      updateAccount(ci);
    }//expelled

  }//inner class ExpelHandler

}//class AccountManager
