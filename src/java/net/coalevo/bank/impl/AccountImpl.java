/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.impl;

import net.coalevo.bank.model.Account;
import net.coalevo.foundation.model.AgentIdentifier;
import net.coalevo.foundation.model.BaseIdentifiable;

/**
 * Provides an implementation of {@link Account}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class AccountImpl
    extends BaseIdentifiable
    implements Account {

  private AgentIdentifier m_AgentIdentifier;
  private long m_LastBalance;
  private long m_Balance;

  public AccountImpl(AgentIdentifier aid) {
    super(aid.getIdentifier());
    m_AgentIdentifier = aid;
  }//AgentBoundCounterImpl

  public AccountImpl(AgentIdentifier aid, long balance) {
    super(aid.getIdentifier());
    m_AgentIdentifier = aid;
    m_Balance = balance;
    m_LastBalance = balance;
  }//AgentBoundCounterImpl

  public AgentIdentifier getAgentIdentifier() {
    return m_AgentIdentifier;
  }//getAgentIdentifier

  public synchronized long getBalance() {
    return m_Balance;
  }//getBalance

  public synchronized long deposit(long l) {
    m_Balance += l;
    return m_Balance;
  }//deposit

  public synchronized long retrieve(long l) {
    long ret = Math.min(m_Balance, l);
    m_Balance -= ret;
    return ret;
  }//retrieve

  public boolean isDirty() {
    return m_Balance != m_LastBalance;
  }//isDirty

  public void setDirty(boolean b) {
    if (!b) {
      m_LastBalance = m_Balance;
    }
  }//setDirty

}//class AccountImpl
