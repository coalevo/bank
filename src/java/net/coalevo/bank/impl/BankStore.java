/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.impl;

import net.coalevo.bank.service.BankConfiguration;
import net.coalevo.datasource.service.DataSourceService;
import net.coalevo.foundation.model.*;
import net.coalevo.foundation.util.ConfigurationMediator;
import net.coalevo.foundation.util.ConfigurationUpdateHandler;
import net.coalevo.foundation.util.metatype.MetaTypeDictionary;
import net.coalevo.foundation.util.metatype.MetaTypeDictionaryException;
import net.coalevo.userdata.service.UserdataService;
import org.apache.commons.pool.BasePoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.osgi.framework.BundleContext;
import org.slamb.axamol.library.Library;
import org.slamb.axamol.library.LibraryConnection;
import org.slamb.axamol.library.SqlUtils;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import javax.sql.DataSource;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.util.NoSuchElementException;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Implements an RDBMS based store for bank data.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BankStore
    implements ConfigurationUpdateHandler {

  private Marker m_LogMarker = MarkerFactory.getMarker(BankStore.class.getName());
  private Library m_SQLLibrary;
  private DataSource m_DataSource;
  private GenericObjectPool m_ConnectionPool;
  private boolean m_New = false;

  // Concurrency mechanisms for backup/restore
  private AtomicInteger m_NumLeased = new AtomicInteger(0);
  private CountDownLatch m_LeaseLatch;
  private CountDownLatch m_RestoreLatch;

  private Messages m_BundleMessages; //Activator.getBundleMessages();

  //Managers
  private AccountManager m_AccountManager;

  public BankStore() {

  }//constructor

  public boolean isNew() {
    return m_New;
  }//isNew

  /**
   * Lease a connection to the underlying database.
   *
   * @return a {@link LibraryConnection} instance.
   * @throws Exception if the connection pool fails or a connection cannot be created.
   */
  public LibraryConnection leaseConnection()
      throws Exception {
    //If a lease latch is set, wait for countdown
    if (m_LeaseLatch != null) {
      m_LeaseLatch.await();
    }
    LibraryConnection lc = (LibraryConnection) m_ConnectionPool.borrowObject();
    m_NumLeased.addAndGet(1);
    return lc;
  }//leaseConnection

  public void releaseConnection(LibraryConnection lc) {
    try {
      m_ConnectionPool.returnObject(lc);
      if (m_NumLeased.decrementAndGet() == 0 && m_RestoreLatch != null) {
        m_RestoreLatch.countDown();
      }
    } catch (Exception e) {
      Activator.log().error(m_LogMarker,"releaseConnection()", e);
    }
  }//releaseConnection

  private synchronized void prepareDataSource() {
    LibraryConnection lc = null;
    try {
      lc = leaseConnection();
      //1. check select
      try {
        lc.executeQuery("existsSchema", null);
      } catch (SQLException ex) {
        m_New = true;
        createSchema(lc);
      }
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"prepareDataSource()", ex);
    } finally {
      releaseConnection(lc);
    }
  }//prepareDataSource

  private boolean createSchema(LibraryConnection lc) {
    try {      //1. Create schema, tables and index
      lc.executeCreate("createSchema");
      lc.executeCreate("createAccountsTable");
      return true;
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"createSchema()", ex);
    }
    return false;
  }//createSchema

  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();
    //1. Configure from persistent configuration
    String ds = "embedded";
    int cpoolsize = 2;
    int accs = 25;


    ConfigurationMediator cm = Activator.getServices().getConfigMediator();
    MetaTypeDictionary mtd = cm.getConfiguration();

    //Retrieve config
    try {
      ds = mtd.getString(BankConfiguration.DATA_SOURCE_KEY);
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("BankStore.activation.configexception", "attribute", BankConfiguration.DATA_SOURCE_KEY),
          ex
      );
    }
    try {
      cpoolsize = mtd.getInteger(BankConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("BankStore.activation.configexception", "attribute", BankConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      accs = mtd.getInteger(BankConfiguration.ACCOUNTS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("BankStore.activation.configexception", "attribute", BankConfiguration.ACCOUNTS_CACHESIZE_KEY),
          ex
      );
    }

    GenericObjectPool.Config poolcfg = new GenericObjectPool.Config();
    poolcfg.maxActive = cpoolsize;
    poolcfg.maxIdle = cpoolsize;
    poolcfg.whenExhaustedAction = GenericObjectPool.WHEN_EXHAUSTED_BLOCK;
    poolcfg.testOnBorrow = true;
    m_ConnectionPool = new GenericObjectPool(new ConnectionFactory(), poolcfg);

    //prepare SQL library and datasource
    try {
      m_SQLLibrary =
          new Library(BankStore.class, "net/coalevo/bank/impl/bankstore-sql.xml");
    } catch (Exception ex) {
      Activator.log().error(m_LogMarker,"activate()", ex);
      return false;
    }
    DataSourceService dss = Activator.getServices().getDataSourceService(ServiceMediator.WAIT_UNLIMITED);
    try {
      m_DataSource = dss.waitForDataSource(ds, -1);
    } catch (NoSuchElementException nse) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("BankStore.activation.datasource"),
          nse
      );
      return false;
    }

    //prepare store
    prepareDataSource();
    Activator.log().info(m_LogMarker,
        m_BundleMessages.get("BankStore.database.info", "source", m_DataSource.toString())
    );

    //Create Managers
    m_AccountManager = new AccountManager(this, accs);

    //add handler config updates
    cm.addUpdateHandler(this);
    return true;
  }//activate

  public synchronized boolean deactivate() {
    if (m_AccountManager != null) {
      //will sync
      m_AccountManager.clear(true);
    }

    //1. close all leased connections
    if (m_ConnectionPool != null) {
      try {
        m_ConnectionPool.close();
      } catch (Exception e) {
        Activator.log().error(m_LogMarker,"deactivate()", e);
      }
    }
    m_AccountManager = null;
    m_DataSource = null;
    m_SQLLibrary = null;
    m_ConnectionPool = null;
    m_BundleMessages = null;
    return true;
  }//deactivate

  public void update(MetaTypeDictionary mtd) {
    //1. Configure from persistent configuration
    int cpoolsize = 2;
    int accs = 25;

    //Retrieve config
    try {
      cpoolsize = mtd.getInteger(BankConfiguration.CONNECTION_POOLSIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("BankStore.activation.configexception", "attribute", BankConfiguration.CONNECTION_POOLSIZE_KEY),
          ex
      );
    }
    try {
      accs = mtd.getInteger(BankConfiguration.ACCOUNTS_CACHESIZE_KEY).intValue();
    } catch (MetaTypeDictionaryException ex) {
      Activator.log().error(m_LogMarker,
          m_BundleMessages.get("BankStore.activation.configexception", "attribute", BankConfiguration.ACCOUNTS_CACHESIZE_KEY),
          ex
      );
    }

    //Update settings
    m_ConnectionPool.setMaxActive(cpoolsize);
    m_ConnectionPool.setMaxIdle(cpoolsize);
    m_AccountManager.setCeiling(accs);
  }//update

  //*** Caches and Managers ***//

  public AccountManager getAccountManager() {
    return m_AccountManager;
  }//getCounterManager


  /**
   * Backups the database of userdata while running.
   * The database will be frozen for writes, but no interruption
   * for reads is required.
   *
   * @param f   the directory to write the backup to.
   * @param tag a possible tag for the backup.
   * @throws SecurityException
   * @throws BackupException   g
   */
  public void backup(File f, String tag)
      throws BackupException {
    /*

    //prepare file
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(f, tag);
    } else {
      backup = f;
    }
    //Get connection
    Connection conn = null;
    try {
      conn = m_DataSource.getConnection();
    } catch (SQLException e) {
      Activator.log().error("doBackup()", e);
      throw new BackupException("BankStore:: Could not obtain db connection.", e);
    }

    //1. Backup Database
    try {
      CallableStatement cs =
          conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)");
      cs.setString(1, backup.getAbsolutePath());
      cs.execute();
      cs.close();
      Activator.log().info(
          m_BundleMessages.get("BankStore.backup.done", "path",
              backup.getAbsolutePath())
      );
    } catch (Exception ex) {
      Activator.log().error("doBackup()", ex);
    }
*/
  }//doBackup

  public void restore(File f, String tag)
      throws RestoreException {
/*
    //1. Latch for future leases
    m_RestoreLatch = new CountDownLatch(1);
    m_LeaseLatch = new CountDownLatch(1);

    try {
      m_RestoreLatch.await();
    } catch (InterruptedException ex) {
      Activator.log().error("restore()", ex);
    }

    //prepare file
    File bdir = new File(f, BankStore.class.getName());
    if (!bdir.exists()) {
      throw new RestoreException("BankStore:: Failed obtain directory of backup.");
    }
    File backup = null;
    if (tag != null && tag.length() > 0) {
      backup = new File(bdir, tag);
    } else {
      backup = bdir;
    }
    //Restore from connection
    //Connection conn = null;
    m_DataSource.setConnectionAttributes(";restoreFrom=" + backup.getAbsolutePath());

    try {
      maintain();
    } catch (MaintenanceException ex) {
      Activator.log().error("restore()", ex);
    } finally {
      m_DataSource.setConnectionAttributes("");
    }

    m_RestoreLatch = null;
    m_LeaseLatch.countDown();
    m_LeaseLatch = null;
    Activator.log().info(m_BundleMessages.get("BankStore.restore.done"));
    */
  }//restore

  public void maintain(ServiceAgent sag) throws MaintenanceException {
    UserdataService uds = Activator.getServices().getUserdataService(ServiceMediator.NO_WAIT);
    if (uds == null) {
      Activator.log().error(
          m_LogMarker, m_BundleMessages.get("maintenance.error.service", "service", "UserdataService")
      );
      return;
    }

    ResultSet rs = null;
    HashMap<String, String> params = new HashMap<String, String>();
    LibraryConnection lc = null;

    try {

      lc = leaseConnection();

      //Clean up accounts for deleted agents
      rs = lc.executeQuery("listAccounts", null);
      while (rs.next()) {
        String aid = rs.getString(1);
        AgentIdentifier a_id = new AgentIdentifier(aid);
        //1. remove deleted
        if (!uds.isUserdataAvailable(sag, a_id)) {
          //Purge if no userdata available
          params.clear();
          params.put("agent_id", aid);
          try {
            lc.executeUpdate("deleteAccount", params);
            Activator.log().info(
                m_LogMarker,
                Activator.getBundleMessages().get(
                    "maintenance.store.removedaccount",
                    "aid",
                    params.get("agent_id")
                )
            );
          } catch (Exception ex) {
            Activator.log().error(
                m_LogMarker,
                Activator.getBundleMessages().get(
                    "maintenance.error.removedaccount",
                    "aid",
                    params.get("agent_id")
                )
            );
          }
        }
      }

    } catch (Exception ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("maintenance.error.store"), ex);
    } finally {
      SqlUtils.close(rs);
      releaseConnection(lc);
    }
  }//maintain

  private class ConnectionFactory
      extends BasePoolableObjectFactory {

    public Object makeObject() throws Exception {
      Connection c = m_DataSource.getConnection();
      c.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
      c.setAutoCommit(true);
      return new LibraryConnection(m_SQLLibrary, c);
    }//makeObject

    public boolean validateObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      try {
        lc.executeQuery("validate",null);
      } catch (SQLException ex) {
        return false;
      }
      return true;
    }//validateObject

    public void destroyObject(Object obj) {
      final LibraryConnection lc = (LibraryConnection) obj;
      if (!lc.isClosed()) {
        SqlUtils.close(lc);
      }
    }//destroyObject

  }//ConnectionFactory

}//class BankStore
