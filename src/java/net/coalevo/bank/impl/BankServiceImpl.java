/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.impl;

import net.coalevo.bank.model.Account;
import net.coalevo.bank.model.AccountStats;
import net.coalevo.bank.model.BankServiceException;
import net.coalevo.bank.model.NoSuchAccountException;
import net.coalevo.bank.service.BankService;
import net.coalevo.foundation.model.*;
import net.coalevo.security.model.NoSuchActionException;
import net.coalevo.security.model.PolicyProxy;
import net.coalevo.security.model.ServiceAgentProxy;
import org.osgi.framework.BundleContext;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Provides an implementation of {@link BankService}.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BankServiceImpl
    extends BaseService
    implements BankService {

  private Marker m_LogMarker = MarkerFactory.getMarker(BankServiceImpl.class.getName());
  private BankStore m_Store;
  private Messages m_BundleMessages;

  private ServiceAgentProxy m_ServiceAgentProxy;
  private PolicyProxy m_PolicyProxy;

  private AccountManager m_AccountManager;

  public BankServiceImpl(BankStore store) {
    super(BankService.class.getName(), ACTIONS);
    m_Store = store;
  }//BankServiceImpl

  public boolean activate(BundleContext bc) {
    m_BundleMessages = Activator.getBundleMessages();

    //1. Authenticate ourself
    m_ServiceAgentProxy = new ServiceAgentProxy(this, Activator.log());
    m_ServiceAgentProxy.activate(bc);

    //2. Policy
    m_PolicyProxy = new PolicyProxy(
        getIdentifier(),
        "COALEVO-INF/security/BankService-policy.xml",
        m_ServiceAgentProxy,
        Activator.log()
    );
    m_PolicyProxy.activate(bc);

    //3. prepare refs
    m_AccountManager = m_Store.getAccountManager();

    return true;
  }//activate

  public boolean deactivate() {
    if (m_PolicyProxy != null) {
      m_PolicyProxy.deactivate();
      m_PolicyProxy = null;
    }
    if (m_ServiceAgentProxy != null) {
      m_ServiceAgentProxy.deactivate();
      m_ServiceAgentProxy = null;
    }

    //Managers
    m_AccountManager = null;
    m_Store = null;
    return true;
  }//deactivate

  public void doMaintenance(Agent caller)
      throws SecurityException, MaintenanceException {

    //1. Security
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), Maintainable.DO_MAINTENANCE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", Maintainable.DO_MAINTENANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }

    //2. Maintenance
    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.begin"));

    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.do.sync"));
    if (m_AccountManager != null) {
      m_AccountManager.sync();
    }

    //3. Store maintainence
    Activator.log().info(m_LogMarker, Activator.getBundleMessages().get("maintenance.do.store"));
    m_Store.maintain(m_ServiceAgentProxy.getAuthenticPeer());

    Activator.log().info(m_LogMarker, m_BundleMessages.get("maintenance.end"));
  }//doMaintainance

  //*** Agent Bound Account Handling ***//

  public boolean existsAccount(Agent caller, AgentIdentifier aid)
      throws SecurityException, BankServiceException {
    if (!caller.getAgentIdentifier().equals(aid)) {

      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), EXISTS_ACCOUNT);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", EXISTS_ACCOUNT.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return false;
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    try {
      return (m_AccountManager.getAccount(aid) != null);
    } catch (NoSuchAccountException ex) {
      return false;
    }
  }//existsAccount

  public void openAccount(Agent caller, AgentIdentifier aid)
      throws SecurityException, BankServiceException {
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), OPEN_ACCOUNT);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", OPEN_ACCOUNT.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return;
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    m_AccountManager.createAccount(aid);
  }//openAccount

  public void closeAccount(Agent caller, AgentIdentifier aid)
      throws SecurityException, BankServiceException {
    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), CLOSE_ACCOUNT);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", CLOSE_ACCOUNT.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return;
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    m_AccountManager.deleteAccount(aid);
  }//closeAccount

  public long getAccountBalance(Agent caller, AgentIdentifier aid)
      throws SecurityException, NoSuchAccountException {

    if (!caller.getAgentIdentifier().equals(aid)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_ACCOUNT_BALANCE);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_ACCOUNT_BALANCE.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return 0;
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }
    return m_AccountManager.getAccount(aid).getBalance();
  }//getAccountBalance

  public long transfer(Agent caller,
                       AgentIdentifier from,
                       AgentIdentifier to,
                       long sum)
      throws SecurityException, BankServiceException {

    if (!caller.getAgentIdentifier().equals(from)) {
      try {
        m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), TRANSFER);
      } catch (NoSuchActionException ex) {
        Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DEPOSIT.getIdentifier(), "caller", caller.getIdentifier()), ex);
        return 0;
      }
    } else {
      m_ServiceAgentProxy.getSecurityService().checkAuthentic(caller);
    }

    Account fromacc = m_AccountManager.getAccount(from);
    Account toacc = m_AccountManager.getAccount(to);
    sum = fromacc.retrieve(sum);
    toacc.deposit(sum);
    return sum;
  }//transfer

  public void deposit(Agent caller, AgentIdentifier to, long sum)
      throws SecurityException, BankServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), DEPOSIT);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", DEPOSIT.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return;
    }

    Account acc = m_AccountManager.getAccount(to);
    acc.deposit(sum);
  }//deposit

  public long retrieve(Agent caller, AgentIdentifier to, long sum) throws SecurityException, BankServiceException {
    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), RETRIEVE);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", RETRIEVE.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return 0;
    }

    Account acc = m_AccountManager.getAccount(to);
    return acc.retrieve(sum);
  }//retrieve

  public AccountStats getAccountStats(Agent caller)
      throws SecurityException, BankServiceException {

    try {
      m_PolicyProxy.ensureAuthorization(m_ServiceAgentProxy.getSecurityService().getAuthorizations(caller), GET_ACCOUNTSTATS);
    } catch (NoSuchActionException ex) {
      Activator.log().error(m_LogMarker, m_BundleMessages.get("error.action", "action", GET_ACCOUNTSTATS.getIdentifier(), "caller", caller.getIdentifier()), ex);
      return null;
    }

    return m_AccountManager.getAccountStats();
  }//getAccountStats

  private static Action OPEN_ACCOUNT = new Action("openAccount");
  private static Action CLOSE_ACCOUNT = new Action("closeAccount");
  private static Action GET_ACCOUNT_BALANCE = new Action("getAccountBalance");
  private static Action EXISTS_ACCOUNT = new Action("existsAccount");
  private static Action DEPOSIT = new Action("deposit");
  private static Action RETRIEVE = new Action("retrieve");
  private static Action TRANSFER = new Action("transfer");

  private static Action GET_ACCOUNTSTATS = new Action("getAccountStats");

  private static Action[] ACTIONS = {
      OPEN_ACCOUNT, CLOSE_ACCOUNT, GET_ACCOUNT_BALANCE, EXISTS_ACCOUNT, GET_ACCOUNTSTATS,
      DEPOSIT, RETRIEVE, TRANSFER,
      Maintainable.DO_MAINTENANCE, Restoreable.DO_BACKUP, Restoreable.DO_RESTORE
  };

}//class BankServiceImpl
