/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.model;

import net.coalevo.bank.service.BankService;

/**
 * Exception thrown when the {@link BankService} fails
 * to provide a given service functionality due to an internal
 * error.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public class BankServiceException
    extends Exception {

  /**
   * Constructs a new <tt>BankServiceException</tt>.
   */
  public BankServiceException() {
  }//constructor

  /**
   * Constructs a new <tt>BankServiceException</tt>.
   *
   * @param cause the root cause for this exception.
   */
  public BankServiceException(Throwable cause) {
    super(cause);
  }//constructor

  /**
   * Constructs a new <tt>BankServiceException</tt>.
   *
   * @param message the message for this exception.
   * @param cause   the root cause for this exception.
   */
  public BankServiceException(String message, Throwable cause) {
    super(message, cause);
  }//constructor

  /**
   * Constructs a new <tt>BankServiceException</tt>.
   *
   * @param message the message for this exception.
   */
  public BankServiceException(String message) {
    super(message);
  }//constructor

}//class BankServiceException
