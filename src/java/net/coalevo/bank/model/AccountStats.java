/***
 * Coalevo Project
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bank.model;

/**
 * Defines an interface for basic account
 * statistics.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface AccountStats {

  /**
   * Returns the average of all accounts.
   *
   * @return the average as double.
   */
  public double getAverage();

  /**
   * Returns the sum of all accounts.
   *
   * @return the sum as long.
   */
  public long getSum();

  /**
   * Returns the minimum value of all accounts.
   *
   * @return the minimum as long.
   */
  public long getMin();

  /**
   * Returns the maximum value of all accounts.
   *
   * @return the max as long.
   */
  public long getMax();

}//interface AccountStats
